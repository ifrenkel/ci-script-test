#!/usr/bin/env sh

report=$1
if [ -z $report ]; then
  echo "Report path is not provided"
  exit 0
fi

max=$2
if [ -z $max ]; then
  echo "Max scan duration was not provided"
  exit 0
fi

valid=`jq '.scan as { start_time: $a, end_time: $b } | [$a, $b] | all(.!=null)' $report`
if [ $valid != true ]; then
  echo "Error processing $report (expected .scan.start_time and .scan.end_time)"
  exit 1
fi

duration=`jq -r 'def conv($t): $t | strptime("%Y-%m-%dT%H:%M:%S") | mktime; .scan as { start_time: $a, end_time: $b } | conv($b)-conv($a)' $report 2>&1`
if [ $? -ne 0 ]; then
  echo "Error processing report ($duration)"
  exit 1
fi

echo "Scan duration: $duration(s) (max: $max(s))"

if [ $duration -gt $max ]; then
  echo "Maximum allowable time for scan duration exceeded"
  exit 1
fi
